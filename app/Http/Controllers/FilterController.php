<?php

namespace App\Http\Controllers;

use App\Http\Requests\FilterRequest;
use App\Services\ArticleService;
use Illuminate\Contracts\View\View;

class FilterController extends Controller {

    private ArticleService $articleService;

    public function __construct(
        ArticleService $articleService,
    ) {
        $this->articleService = $articleService;
    }

    public function filterByCategory(FilterRequest $request): View {
        $articles = $this->articleService->filterByCategory($request);

        return view('article.filter', compact('articles'));
    }

}
