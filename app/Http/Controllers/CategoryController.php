<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Services\CategoryService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class CategoryController extends Controller {

    private CategoryService $categoryService;

    public function __construct(
        CategoryService $categoryService
    ) {
        $this->categoryService = $categoryService;
    }

    public function create(): View {
        $categories = $this->categoryService->all();

        return view('category.create', compact('categories'));
    }

    public function save(CategoryRequest $request): RedirectResponse {
        $this->categoryService->create($request->validated());

        return redirect()->route('category.create');
    }

    public function delete(int $id): RedirectResponse {
        $category = $this->categoryService->findById($id);
        $category->delete();

        return redirect()->route('category.create');
    }

}
