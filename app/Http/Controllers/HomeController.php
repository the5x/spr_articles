<?php

namespace App\Http\Controllers;

use App\Services\ArticleService;
use App\Services\CategoryService;
use Illuminate\Contracts\View\View;

class HomeController extends Controller {

    private ArticleService $articleService;
    private CategoryService $categoryService;

    public function __construct(
        ArticleService $articleService,
        CategoryService $categoryService
    ) {
        $this->articleService = $articleService;
        $this->categoryService = $categoryService;
    }

    public function index(): View {
        $articles = $this->articleService->orderByDate();
        $categories = $this->categoryService->all();

        return view('welcome', compact('articles', 'categories'));
    }

}
