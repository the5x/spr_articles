<?php

namespace App\Http\Controllers;

use App\Services\ArticleService;
use Illuminate\Contracts\View\View;

class ManagerController extends Controller {

    private ArticleService $articleService;

    public function __construct(
        ArticleService $articleService
    ) {
        $this->articleService = $articleService;
    }

    public function index(): View {
        $articles = $this->articleService->orderByDate();

        return view('manager.index', compact('articles'));
    }

}
