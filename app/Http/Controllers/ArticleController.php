<?php

namespace App\Http\Controllers;

use App\Http\Requests\ArticleRequest;
use App\Services\ArticleService;
use App\Services\CategoryService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class ArticleController extends Controller {

    private ArticleService $articleService;
    private CategoryService $categoryService;

    public function __construct(
        ArticleService $articleService,
        CategoryService $categoryService
    ) {
        $this->articleService = $articleService;
        $this->categoryService = $categoryService;
    }

    public function create(): View {
        $categories = $this->categoryService->all();

        return view('article.create', compact('categories'));
    }

    public function edit(int $id): View {
        $article = $this->articleService->findById($id);
        $categories = $this->categoryService->all();

        return view('article.edit', compact('article', 'categories'));
    }

    public function show(int $id): View {
        $article = $this->articleService->findById($id);

        return view('article.show', compact('article'));
    }

    public function save(ArticleRequest $request): RedirectResponse {
        $article = $this->articleService->create($request->validated());
        $article->category()->associate($request->category_id)->save();

        return redirect()->route('manager.index');
    }

    public function update(ArticleRequest $request, $id): RedirectResponse {
        $article = $this->articleService->findById($id);
        $article->update($request->validated());

        return redirect()->route('manager.index');
    }

    public function delete($id): RedirectResponse {
        $article = $this->articleService->findById($id);
        $article->delete();

        return redirect()->route('manager.index');
    }

}
