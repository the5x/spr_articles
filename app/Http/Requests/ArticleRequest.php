<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticleRequest extends FormRequest {

    public function authorize(): bool {
        return true;
    }

    public function rules(): array {
        return [
            'title' => 'required|string|max:190',
            'short' => 'required|string|max:300',
            'description' => 'required|string|max:500',
            'category_id' => 'required|exists:categories,id',
        ];
    }

}
