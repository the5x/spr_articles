<?php


namespace App\Repositories;


use App\Models\Category;

class CategoryRepository {

    private Category $categoryModel;

    public function __construct(
        Category $categoryModel
    ) {
        $this->categoryModel = $categoryModel;
    }

    public function create(array $data) {
        return $this->categoryModel::create($data);
    }

    public function findById(int $id) {
        return $this->categoryModel::where('id', $id)
            ->firstOrFail();
    }

    public function all() {
        return $this->categoryModel::all();
    }

}
