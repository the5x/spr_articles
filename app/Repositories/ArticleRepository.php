<?php


namespace App\Repositories;


use App\Models\Article;
use Illuminate\Database\Eloquent\Builder;

class ArticleRepository {

    private Article $articleModel;

    public function __construct(
        Article $articleModel
    ) {
        $this->articleModel = $articleModel;
    }

    public function orderByDate() {
        return $this->articleModel::orderBy('created_at', 'DESC')
            ->get();
    }

    public function create(array $data) {
        return $this->articleModel::create($data);
    }

    public function findById(int $id) {
        return $this->articleModel::where('id', $id)
            ->firstOrFail();
    }

    public function filterByCategory($data) {
        $articles = $this->articleModel::with(['category']);

        if ($data->has('category_id')) {
            $articles = $articles->whereHas('category', function (Builder $query) use ($data) {
                $query->where('category_id', $data->input('category_id'));
            });
        }

        return $articles->get();
    }

}
