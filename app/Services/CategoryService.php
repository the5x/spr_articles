<?php


namespace App\Services;


use App\Repositories\CategoryRepository;

class CategoryService {

    private CategoryRepository $categoryRepository;

    public function __construct(
        CategoryRepository $categoryRepository
    ) {
        $this->categoryRepository = $categoryRepository;
    }

    public function create(array $data) {
        return $this->categoryRepository
            ->create($data);
    }

    public function findById(int $id) {
        return $this->categoryRepository
            ->findById($id);
    }

    public function all() {
        return $this->categoryRepository
            ->all();
    }

}
