<?php


namespace App\Services;


use App\Repositories\ArticleRepository;

class ArticleService {

    private ArticleRepository $articleRepository;

    public function __construct(
        ArticleRepository $articleRepository
    ) {
        $this->articleRepository = $articleRepository;
    }

    public function orderByDate() {
        return $this->articleRepository
            ->orderByDate();
    }

    public function create(array $data) {
        return $this->articleRepository
            ->create($data);
    }

    public function findById(int $id) {
        return $this->articleRepository
            ->findById($id);
    }

    public function filterByCategory($data) {
        return $this->articleRepository
            ->filterByCategory($data);
    }

}
