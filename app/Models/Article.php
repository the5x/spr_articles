<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Article extends Model {

    use HasFactory;

    protected $table = 'articles';
    public $timestamps = false;
    protected $fillable = ['title', 'description', 'category_id', 'short'];

    public function category(): BelongsTo {
        return $this->belongsTo(Category::class);
    }

}
