1. Install MySQL 8, PHP 8.1, Laravel 9.1 with Sail + docker-compose.yml config
2. Composer update from Laravel 8.8 to 9.2

start local:
1. git clone https://gitlab.com/the5x/spur.git
2. cd spur
3. ./vendor/bin/sail up -d
4. ./vendor/bin/sail composer install
5. ./vendor/bin/sail artisan key:generate
6. Add your database config in the .env file
7. ./vendor/bin/sail artisan migrate
8. ./vendor/bin/sail artisan db:seed
