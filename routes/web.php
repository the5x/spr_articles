<?php

use App\Http\Controllers\ArticleController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\FilterController;
use App\Http\Controllers\ManagerController;
use Illuminate\Support\Facades\Route;

// Home
Route::get('/', [HomeController::class, 'index'])->name('home');

// Manager panel
Route::get('/manager', [ManagerController::class, 'index'])->name('manager.index');

// Filter by category
Route::get('/filter', [FilterController::class, 'filterByCategory'])->name('filter');

// Article CRUD
Route::get('/articles/create', [ArticleController::class, 'create'])->name('article.create');
Route::post('/articles/save', [ArticleController::class, 'save'])->name('article.save');
Route::get('/articles/{id}', [ArticleController::class, 'show'])->name('article.show');
Route::get('/articles/{id}/edit', [ArticleController::class, 'edit'])->name('article.edit');
Route::put('/articles/{id}/edit', [ArticleController::class, 'update'])->name('article.update');
Route::delete('/articles/{id}', [ArticleController::class, 'delete'])->name('article.delete');

// Category CRUD
Route::get('/categories/create', [CategoryController::class, 'create'])->name('category.create');
Route::post('/categories/save', [CategoryController::class, 'save'])->name('category.save');
Route::delete('/categories/{id}', [CategoryController::class, 'delete'])->name('category.delete');
