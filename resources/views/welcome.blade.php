@extends('layouts.default')
@section('content')
    <a class="breadcrumbs" href="{{ route('manager.index') }}">Вернуться в менеджер</a>

    <form action="{{ route('filter') }}" method="GET">
        <label class="form__label" for="category_id">Фильтр по категориям</label>
        <select class="form__select" name="category_id" id="category_id">
            @foreach($categories as $category)
                <option class="form__option" value="{{ $category->id }}">{{ $category->name  }}</option>
            @endforeach
        </select>

        <button class="button" type="submit">Найти</button>
    </form>

    @foreach($articles as $article)
        <article class="article">
            <a class="article__link"
               href="{{ route('article.show', ['id' => $article->id]) }}">{{ $article->title }}</a>
        </article>
    @endforeach

@stop
