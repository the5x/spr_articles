<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('includes.header')
    <title>@yield('title')</title>
</head>
<body>
<main class="wrapper">
    @yield('content')
</main>
</body>
</html>
