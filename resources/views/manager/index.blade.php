@extends('layouts.default')
@section('title', 'Панель управления')
@section('content')
    <a class="breadcrumbs" href="{{ route('home') }}">Вернуться на главную</a>

    <ul class="list">
        <li class="list__item"><a class="list__link" href="{{ route('article.create') }}">Создать новость</a></li>
        <li class="list__item"><a class="list__link" href="{{ route('category.create') }}">Создать категорию</a></li>
    </ul>

    <ol class="list">
        @foreach($articles as $article)
            <li class="list__item">
                <form action="{{ route('article.delete', $article->id) }}" method="POST">
                    @method('DELETE')
                    @csrf

                    <button class="btn" type="submit">x</button>
                </form>

                <a class="list__link list__link--big" href="{{ route('article.show', $article->id) }}">{{ $article->title }}</a>

                <a class="list__link list__link--color"
                   href="{{ route('article.edit', $article->id) }}">редактировать</a>
            </li>
        @endforeach
    </ol>

@stop
