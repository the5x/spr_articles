@extends('layouts.default')
@section('title', 'Фильтр по категории')
@section('content')
    <a class="breadcrumbs" href="{{ route('home') }}">Вернуться на главную</a>

    <strong class="article__result">Количество найденных: {{ $articles->count() }}</strong>

    <ol class="list">
        @foreach($articles as $article)
            <li class="list__item">
                <a class="list__link article__link" href="{{ route('article.show', $article->id) }}">{{ $article->title }}</a>
            </li>
        @endforeach
    </ol>

@stop
