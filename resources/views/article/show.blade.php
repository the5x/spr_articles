@extends('layouts.default')
@section('title', $article->title)
@section('content')
    <a class="breadcrumbs" href="{{ route('manager.index') }}">Вернуться в менеджер</a>

    <article class="article">
        <h1 class="article__title">{{ $article->title }}</h1>
        <small class="article__category">{{ $article->category->name }}</small>
        <em class="article__short">{{ $article->short }}</em>
        <p class="article__description">{{ $article->description }}</p>
    </article>
@stop
