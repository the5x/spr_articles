@extends('layouts.default')
@section('title', 'Редактировать статью')
@section('content')
    <a class="breadcrumbs" href="{{ route('manager.index') }}">Вернуться в менеджер</a>

    <form action="{{ route('article.update', $article->id) }}" method="POST">
        @method('PUT')
        @csrf

        @error('category_id')
        <small class="error">{{ $message }}</small>
        @enderror
        <label class="form__label" for="category_id">Категории
            <select class="form__select" name="category_id" id="category">
                @foreach($categories as $category)
                    <option class="form__option" value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
            </select>
        </label>

        @error('title')
        <small class="error">{{ $message }}</small>
        @enderror
        <label class="form__label" for="title">Заголовок
            <input class="form__input" value="{{ old('title') ?? $article->title }}" type="text" name="title"
                   maxlength="190">
        </label>

        @error('short')
        <small class="error">{{ $message }}</small>
        @enderror
        <label class="form__label" for="short">Краткое описание
            <textarea class="form__textarea" name="short" id="short" cols="30"
                      rows="10">{{ old('short') ?? $article->short }}</textarea>
        </label>

        @error('description')
        <small class="error">{{ $message }}</small>
        @enderror
        <label class="form__label" for="description">Описание
            <textarea class="form__description" name="description" id="description" cols="30"
                      rows="10">{{ old('description') ?? $article->description }}</textarea>
        </label>

        <button class="button" type="submit">Обновить</button>
    </form>

@stop
