@extends('layouts.default')
@section('title', 'Категории')
@section('content')
    <a class="breadcrumbs" href="{{ route('manager.index') }}">Вернуться в менеджер</a>

    <ol class="list">
        @foreach($categories as $category)
            <li class="list__item">
                <form action="{{ route('category.delete', $category->id) }}" method="POST">
                    @method('DELETE')
                    @csrf

                    <button class="btn" type="submit">x</button>
                </form>

                {{ $category->name }}
            </li>
        @endforeach
    </ol>

    <article class="article">
        <form action="{{ route('category.save') }}" method="POST">
            @csrf

            @error('name')
            <small class="error">{{ $message }}</small>
            @enderror
            <label class="form__label" for="name">Название
                <input class="form__input" value="{{ old('name') }}" type="text" name="name" maxlength="190">
            </label>

            <button class="button" type="submit">Создать</button>
        </form>
    </article>
@stop
